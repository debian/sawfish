; change cursors when moving or resizing a window
(setq move-cursor-shape 'hand2)   
(setq resize-cursor-shape 'hand2)

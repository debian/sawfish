<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

  <!ENTITY dhfirstname "<firstname>Christian</firstname>">
  <!ENTITY dhsurname   "<surname>Marillat</surname>">
  <!ENTITY dhdate      "<date>april  7, 2001</date>">
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>marillat@debian.org</email>">
  <!ENTITY dhusername  "Christian Marillat">
  <!ENTITY dhucpackage "<refentrytitle>sawfish-ui</refentrytitle>">
  <!ENTITY dhpackage   "sawfish-ui">
  <!ENTITY debian      "<productname>Debian GNU/Linux</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2001</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>The Sawfish Configurator.</refpurpose>

  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>

     <arg><option> --group=<replaceable>GROUP-NAME </replaceable></option></arg>
     <arg><option> --flatten </option></arg>
     <arg><option> --single-level </option></arg>
     <arg><option> --socket-id=<replaceable>WINDOW-ID </replaceable></option></arg>

    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This program can be used to invoke the GUI manually; if GNOME is being
      used, then the GNOME Control Center can also be used to customize
      certain classes.</para>

  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <variablelist>
      <varlistentry>
        <term><option>--group</option>
	  <option>GROUP-NAME</option>
	</term>
        <listitem>
          <para>Display only <replaceable>GROUP-NAME</replaceable></para>

          <para><replaceable>GROUP-NAME</replaceable> should be one of these
            names Sawfish, Appearance, Bindings, Focus, Matched Windows,
            Minimizing/Maximizing, Miscellaneous, Move/Resize, Placement, Sounds, Workspaces</para>

        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--flatten</option></term>
        <listitem>
          <para>display sub-groups inline instead of using a tree widget
            (good for control center). In normal `tree' item, don't put frame
            the widgets</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--single-level</option></term>
        <listitem>
          <para>Don't display the tree widget.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--socket-id</option>
	  <option>WINDOW-ID</option>
	</term>
        <listitem>
          <para>This option allow the ui to be embedded in the gnome control
            center. This also disables the ok/cancel/etc buttons and enables
            a hacky protocol on stdin/stdout</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others).</para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
sgml-set-face:non-nil
End:
-->
